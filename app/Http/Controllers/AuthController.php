<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
use JWTAuth;
use Response;
use JWTFactory;

class AuthController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['login']]);
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'user' => 'required',
            'password' => 'required'
        ]);
        $user = User::where('user', '=', $request->user)->first();
        if(!$user){
            return Response::json([
                'message' => 'Tên tài khoản không đúng'
            ], 404);
        }
        if(Hash::check($request->password, $user->password)){
            $credentials = $request->only('user', 'password');
            $credentials['id'] = $user->id;
            $token = $this->authenticate($credentials);
            return Response::json(compact('token'));
        }
        return Response::json([
            'message' => 'Mật khẩu không đúng'
        ], 406);
    }

    public function logout(Request $request)
    {
        $token = JWTAuth::getToken();
        if($token){
            JWTAuth::setToken($token)->invalidate();
            return Response::json([
                'message' => 'Logout successfully'
            ], 200);
        }
    }

    public function authenticate($credentials)
    {
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return $token;
    }

    public function getAuthenticatedUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json(compact('user'));
    }
}

