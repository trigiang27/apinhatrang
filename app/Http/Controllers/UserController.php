<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
use Response;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['store']]);
    }

    public function index(Request $request)
    {
        $search_term = $request->search;
        $limit = $request->limit? $request->limit : 5;
        if($search_term){
            $users = User::where('user', 'LIKE', "%$search_term%")->select('id', 'user', 'email')->orderBy('id', 'DESC')->paginate($limit);
            $users->appends(array(
                'search' => $search_term,
                'limit' => $limit
            ));
        } else {
            $users = User::select('id', 'user', 'email')->orderBy('id', 'DESC')->paginate($limit);
            $users->appends(array(
                'limit' => $limit
            ));
        }
        return Response::json($users, 200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'user' => 'required|unique:users,user|min:5|max:30',
            'email' => 'required|email|min:8|max:50',
            'password' => 'required|min:8|max:30',
            'rePassword' => 'required|same:password'
        ],
        [
            'user.required' => 'Vui lòng cung cấp user',
            'user.unique' => 'Tên đăng nhập đã tồn tại',
            'user.min' => 'User không được ít hơn 5 ký tự',
            'user.max' => 'User không được dài hơn 30 ký tự',
            'email.required' => 'Vui lòng cung cấp email',
            'email.email' => 'Email không đúng, vui lòng kiểm tra lại',
            'email.min' => 'Email không được ít hơn 8 ký tự',
            'email.max' => 'Email không được dài hơn 50 ký tự',
            'password.required' => 'Vui lòng cung cấp mật khẩu',
            'password.min' => 'Mật khẩu không được ít hơn 8 ký tự',
            'password.max' => 'Mật khẩu không được dài hơn 30 ký tự',
            'rePassword.required' => 'Vui lòng xác nhận mật khẩu',
            'rePassword.same' => 'Xác nhận mật khẩu không đúng'
        ]);
        $user = User::where('user', '=', $request->user)->get();
        $user = new User;
        $user->fill($request->all());
        $user->password = Hash::make($request->password);
        $user->save();
        return Response::json([
            'message' => 'Tạo tài khoản thành công',
            'data' => $user
        ], 200);
    }

    public function show($user)
    {
        $data_token = JWTAuth::parseToken()->authenticate();
        if($data_token->user != $user){
            return Response::json([
                'message' => 'Bạn không có quyền sửa tài khoản này'
            ], 400);
        }
        $user = User::find($data_token->id);
        if(!$user){
            return Response::json([
                'message' => 'User does not exits'
            ], 404);
        }
        $previous = User::where('id', '<', $user->id)->max('id');
        $next = User::where('id', '>', $user->id)->min('id');
        return Response::json([
            'previous_user_id'=> $previous,
            'next_user_id'=> $next,
            'data' => $user
        ], 200);
    }

    public function update(Request $request, $user)
    {
        $data_token = JWTAuth::parseToken()->authenticate();
        if($data_token->user != $user){
            return Response::json([
                'message' => 'Bạn không có quyền sửa tài khoản này'
            ], 400);
        }
        $user = User::find($data_token->id);
        if(!$user){
            return Response::json([
                'message' => 'Tên tài khoản không tồn tại'
            ], 404);
        }
        if(!$request->password or !$request->rePassword){
            return Response::json ([
                'message' => 'Vui lòng cung cấp mật khẩu và xác nhận mật khẩu'
            ], 422);
        }
        if($request->password != $request->rePassword){
            return Response::json ([
                'message' => 'Xác nhận mật khẩu không đúng'
            ], 400);
        }
        if($request->email){
            $user->email = $request->email;
        }
        $user->password = Hash::make($request->password);
        $user->save();
        $user = User::select('id', 'user', 'email')->where('id', '=', $user->id)->get();
        return Response::json([
            'message' => 'Tài khoản được cập nhật thành công',
            'data' => $user
        ], 200);
    }

    public function destroy($user)
    {
        $data_token = JWTAuth::parseToken()->authenticate();
        if($data_token->user != $user){
            return Response::json([
                'message' => 'Bạn không có quyền sửa tài khoản này'
            ], 400);
        }
        $user = User::find($data_token->id);
        if(!$user){
            return Response::json([
                'message' => 'Tên tài khoản không tồn tại'
            ], 404);
        }
        $user->delete();
        return Response::json([
            'message' => 'Tài khoản đã được xóa'
        ], 200);
    }
}
